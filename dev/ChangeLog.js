//版本号含义: 主版本号.子版本号.修正版本号

//版本日志
changeLog = {};
changeLog.dev = {};
changeLog.beta = {};
changeLog.stable = {};

//开发版
changeLog.dev['1.0.1'] = '初步设计完成结构.';
changeLog.dev['1.0.2'] = 'Selector开发完成';

//测试版
changeLog.beta['1.0.x'] = '';

//稳定版
changeLog.stable['1.0.x'] = '';
