/**
 * 描述样式的类
 */
JCSSEdit.Declaration = function(){

    /**
     * 样式列表
     * null视为不存在的样式
     */
    var list = [];

    /**
     * 样式数量
     */
    var count = 0;

    /**
     * 检查参数是否合法
     */
    var checkArg = JCSSEdit.public.checkStringArg;

    /**
     * 从list中寻找指定name的元素
     */
    var findByName = JCSSEdit.public.findObjectFromArray;

    /**
     * 添加一个样式
     * @param name 样式名
     * @param value 样式值
     */
    this.add = function(name, value){
        if(checkArg(name) && checkArg(value) && !this.has(name)) {
            list.push(
                {
                    'name': name,
                    'value': value
                }
            );
            count++;
        }
    };

    /**
     * 删除一个选择器
     * @param name 选择器名称
     */
    this.del = function(name) {
        if(checkArg(name)) {
            var id = findByName(list, 'name', name);
            if(id >= 0){
                list[id] = undefined;
                count--;
            }
        }
    };

    /**
     * 判断是否存在某个样式
     * @param name 样式名称
     * @return boolean 是否存在指定样式
     */
    this.has = function(name) {
        if(checkArg(name)){
            return (findByName(list, 'name', name) >= 0);
        }

        return false;
    };

    /**
     * 获取现有的选择器个数
     * @return number 选择器个数
     */
    this.len = function() {
        return count;
    };

    this.debug = function(){
        return list;
    };
};