JCSSEdit = {};

/**
 * 公共方法 & 常量
 */
JCSSEdit.public = {};

/**
 * 检查参数是否合法
 * @param arg 要检查的参数
 * @return boolean 该参数是否合法
 */
JCSSEdit.public.checkStringArg = function(arg){
    return (
        typeof arg === 'string' &&
        arg !== undefined &&
        arg !== null &&
        arg !== ''
    );
};

/**
 * 在存储了对象的数组中按照属性名查找元素
 * @param arr 目标数组
 * @param name 属性名
 * @param value 属性值
 * @return boolean 找到的第一个元素的id 如未找到则返回-1
 */
JCSSEdit.public.findObjectFromArray = function(arr, name, value){
    for(var id in arr) {
        if(arr[id][name] === value) {
            return id;
        }
    }

    return -1;
};

console.log('CSSEdit Version: 1.0.0.0 Dev');
console.log('引擎开发中, 请勿用于实际生产环境.');
