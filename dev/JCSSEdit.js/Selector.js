/**
 * 描述选择器的类
 */
JCSSEdit.Selector = function() {

    /**
     * 选择器列表
     * 均为string
     * undefined视为不存在的元素
     */
    var list = [];

    /**
     * 选择器数量
     */
    var count = 0;

    /**
     * 检查参数是否合法
     */
    var checkArg = JCSSEdit.public.checkStringArg;

    /**
     * 添加一个选择器
     * @param name 选择器名称
     */
    this.add = function(name) {
        if(checkArg(name) && list.indexOf(name) === -1) {
            list.push(name);
            count++;
        }
    };

    /**
     * 删除一个选择器
     * @param name 选择器名称
     */
    this.del = function(name) {
        if(checkArg(name)) {
            var id = list.indexOf(name);
            if(id >= 0){
                list[id] = undefined;
                count--;
            }
        }
    };

    /**
     * 判断是否存在某个选择器
     * @param name 选择器名称
     * @return boolean 是否存在指定选择器
     */
    this.has = function(name) {
        return checkArg(name) ? (list.indexOf(name) >= 0) : false;
    };

    /**
     * 获取现有的选择器个数
     * @return number 选择器个数
     */
    this.len = function() {
        return count;
    };

    /**
     * 获取现有的选择器列表
     * @return Array 选择器列表
     */
    this.getArray = function() {
        var temp = [];
        for(var id in list) {
            if(list[id] !== undefined) {
                temp.push(list[id]);
            }
        }
        return temp;
    };

    /**
     * 获取现有的选择器字符串表达形式, 一般用于最终拼合CSS文件
     * @return Array 选择器字符串表达形式
     */
    this.toString = function() {
        var str = '';
        for(var id in list) {
            if(list[id] !== undefined) {
                str = str.concat(list[id], ' ');
            }
        }
        return str.substring(0, str.length - 1);
    };

    /**
     * 清理无用资源, 仅应在多次执行del后调用
     */
    this.clearMem = function() {
        list = this.getArray();
    };
};
