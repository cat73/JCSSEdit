//这是目前计划的结构

css = { //主容器
    'list': [ //css列表
        { //一条具体的CSS
            'Selector': { //选择器列表
                'list': ['.table', '.test']
            },
            'Declaration': { //样式列表
                'list': [
                    {'name': 'width', 'value': '80%'},
                    {'name': 'color', 'value': '#0F0'}
                ]
            }
        },
        { //一条具体的CSS
            'Selector': { //选择器列表
                'list': ['html', 'span']
            },
            'Declaration': { //样式列表
                'list': [
                    {'name': 'background-color', 'value': '#428bca'}
                ]
            }
        }
    ]
};